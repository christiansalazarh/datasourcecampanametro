SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `dscampanametro`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dscampanametro` ;

CREATE  TABLE IF NOT EXISTS `dscampanametro` (
  `cedula` CHAR(11) NOT NULL ,
  `nombres` VARCHAR(100) NULL ,
  `direccion` VARCHAR(100) NULL ,
  `telefono` VARCHAR(100) NULL ,
  `fecha_act` CHAR(16) NULL ,
  `cuotas` BLOB NULL ,
  PRIMARY KEY (`cedula`) )
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
