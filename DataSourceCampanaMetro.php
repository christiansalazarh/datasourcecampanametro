<?php
/***
  	install as component:
		
	protected/config/main.php
	..
	'components'=>array(
		'dsx'=>array(
			'class'=>'ext.datasourcecampanametro.DataSourceCampanaMetro',
			'datasource'=>'vpn',
			'vpn_view_name'=>'nombre_de_la_vista_que_el_cliente_da',
			'vpn_db'=>array(
				'connectionString' => 'mysql:host=localhost;dbname=vpndbname',
				'emulatePrepare' => true,
				'username' => 'bla',
				'password' => 'xxblaxx',
				'charset' => 'utf8',
			),
		),
	),

 * @author Christian Salazar H. <christiansalazarh@gmail.com>
*/
class DataSourceCampanaMetro 
	extends CApplicationComponent
	implements IDataSourceCampana {
	
	public $datasource; // "vpn" or "csv"
	public $vpn_db;	// db settings. required when datasource is "vpn"
	public $vpn_view_name;
	private $_db;
	private $_last_key;
	private $_last_record;
	public function validaFormatoCuotas($cuotas){
		$cuotas = trim($cuotas,"; ").";";
		$ex = "/^(([0-9]{1,6})=Q([0-9]{1,2})-([0-9]+(\.[0-9]+)?);)+$/";
		if(!preg_match($ex,$cuotas))
			return false;
		return $cuotas;
	}
	public function leerCliente($cedula){
		if($this->_last_key == $cedula)
			return $this->_last_record;//LAZY
		if($this->_last_record = $this->getDb()->
			createCommand()->select()->from($this->_Tablename())	
			->where("cedula=:c",array(":c"=>$cedula))->queryRow()){
			return $this->_last_record;
		}else{
			$this->_last_record = null;
			return null;
		}
	}
	public function listarContratos($cedula){
		if($row = $this->leerCliente($cedula)){
			$contracts = array();
			foreach($this->decodeQuotes($row['cuotas']) as $quote){
				list($qn, $contract, $val1, $val2, $date) = $quote;
				if(!in_array($contract, $contracts))
					$contracts[] = $contract;
			}
			return $contracts;
		}else
		return array();
	}
	public function listarCuotas($cedula, $numero_contrato){
		if($row = $this->leerCliente($cedula)){
			$quotes = array();
			foreach($this->decodeQuotes($row['cuotas']) as $quote){
				list($qn, $contract, $val1, $val2, $date) = $quote;
				if($contract == $numero_contrato)
					$quotes[] = $quote;
			}
			return $quotes;
		}else
		return array();
	}
	/**
	 * validarCuotasAPagar 
	 *	valida que las cuotas a ser pagadas ($cuotas) esten definidas en la data original de la cedula
	 * 
	 * @param mixed $cedula 
	 * @param mixed $cuotas "NUMEROCONTRATO=QN-MONTOQPAGA.DECIMAL;...;...;"
	 * @access public
	 * @return bool
	 */
	public function validaCuotasAPagar($cedula, $cuotas,&$error){
		if(!$this->leerCliente($cedula)){
			$error="cedula no existente: ".$cedula;
			return false;
		}
		foreach(explode(";",rtrim($cuotas,";")) as $item){
			//$item es: contrato=cuota-monto
			$k = explode("=",$item);
			$contrato = $k[0];
			$z = explode("-",$k[1]);
			$cuota = $z[0];
			$monto = $z[1];
			//
			$ok_contrato=false;
			$ok_cuota=false;
			foreach($this->listarCuotas($cedula, $contrato) as $quote){
				$ok_contrato=true;
				list($qn, $contract, $val1, $val2, $date) = $quote;
				if($qn == $cuota){
					$ok_cuota=true;
					if(round($monto,2) == round($val2,2)){
						// monto OK
					}else{
						$error="el valor a pagar (".$monto.") debe coincidir con el valor de la cuota (".$val2.")";
						return false;
					}	
				}
			}
			if($ok_contrato == false){
				$error="contrato ".$contrato." no hallado para cedula ".$cedula;
				return false;
			}
			if($ok_cuota == false){
				$error="numero de cuota: ".$cuota." es inexistente en contrato: ".$contrato;
				return false;
			}
		}
		return true;
	}
	public function importarCsv($filename,$delim=";"){
		if(!is_file($filename))	
			return "ERR_NOT_A_FILE";
		// force db connection to be "CSV" when importing data
		$error="";
		$this->_db = null;
		$this->datasource = "csv";
		$this->getDb()->createCommand()->delete($this->_Tablename());
		$f = fopen($filename,"r");
		$n=0;
		while($data = fgetcsv($f, 0, $delim)){
			list($ced, $nom, $dir, $tel, $fec, $cuotas)=$data;
			if(($n==0) && (strtolower($ced)=="cedula")) { $n++; continue; }
			$this->getDb()->createCommand()->insert($this->_Tablename(),
				array(
					"cedula"=>$ced,
					"nombres"=>$nom,
					"direccion"=>$dir,
					"telefono"=>$tel,
					"fecha_act"=>$fec,
					"cuotas"=>$cuotas
				));
			$n++;
		}
		fclose($f);
		if($error) return "ERROR";
		return "OK";
	}
	public function countPages($items_per_page,&$count=null){
		$rows = $this->getDb()->createCommand()->
			select('count(cedula) cnt')->
			from($this->_Tablename())->queryAll();
		$count = 1*($rows[0]['cnt']);
		if($rows != null){
			$pages = round($count / $items_per_page);
			if(($pages * $items_per_page) < $rows[0]['cnt']) $pages++;     	
			return $pages;
		}else
		return 0;
	}
	public function getPage($n,$items_per_page,&$pages=null){
		if($pages == null)
			$pages = $this->countPages($items_per_page);
		if(!$pages) return array();
		$offset = $items_per_page * $n;
		return $this->getDb()->createCommand()->select()->
			from($this->_Tablename())->
			offset($offset)->limit($items_per_page)->queryAll();
	}
	private function getDb(){
		if($this->_db == null){
			if($this->datasource == "csv"){
				$this->_db = Yii::app()->db;
			}else{
				// build a db connection.
				$this->_db = new CDbConnection(
					$this->vpn_db['connectionString'], 
						$this->vpn_db['username'],$this->vpn_db['password']);
				if(isset($this->vpn_db['charset']))
					$this->_db->charset = $this->vpn_db['charset'];
				if(isset($this->vpn_db['emulatePrepare']))
					$this->_db->emulatePrepare = $this->vpn_db['emulatePrepare'];
				$this->_db->active = true;
			}
		}
		if($this->_db == null)
			throw new Exception("invalid database connection");
		return $this->_db;
	}
	private function _Tablename(){
		if($this->datasource=="csv")
			return "dscampanametro";
		if($this->datasource=="vpn")
			return $this->vpn_view_name;
		return "";
	}
	private function decodeQuotes($quotes){
		$out=array();
		foreach(explode(";",$quotes) as $quote){
			if($quote != ""){
			list($qn,$items) = explode("=",$quote);
			list($contract, $val1, $val2, $date) = explode(",",$items);
			$dt=explode("/",$date);
			$out[] = array($qn,$contract, $val1, $val2, 
				date("Y-m-d",strtotime(sprintf("%s-%s-%s",$dt[2],$dt[1],$dt[0]))));
			}
		}
		return $out;
	}
	private function _sampleRecord(){
		$quotes = 
			 "Q1=021546,75.12,84.1344,26/12/2004;"
			."Q2=021546,88.17,98.7504,26/12/2005;"
			."Q3=021546,102,114.24,26/12/2006;"
			."Q4=021546,117,131.04,26/12/2007;"
			."Q5=021546,143.13,160.306,26/12/2008;"
			."Q6=021546,189.9,212.688,26/12/2009;"
			."Q1=021547,241.08,270.01,26/12/2010;"
			."Q2=021547,308.04,345.005,26/12/2011;"
			."Q1=021548,388.41,435.019,26/12/2012;"
			."Q2=021548,575.88,644.986,26/12/2013;";
		return array(
			"cedula"=>'TEST',
			"nombres"=>'PORCUPINE TREE',
			"direccion"=>'ARRIVING SOMEWHERE, BUT NOT HERE',
			"telefono"=>'555-9-123456;555-8-876543',
			"fecha_act"=>'2013-08-20',
			"cuotas"=>$quotes
		);
	}
	public function test($vpn_db){
		// called by consolecommand to test features
		$this->_db = null;
		$this->datasource = "csv";
		printf("tableName is: %s\n",$this->_Tablename());
		printf("getDb call when datasource is: %s...",$this->datasource);
		$this->getDb();
		printf("OK\n");

		$this->_db = null;
		$this->vpn_db = $vpn_db;
		$this->datasource = "vpn";
		printf("viewName is: %s\n",$this->_Tablename());
		printf("getDb call when datasource is: %s...",$this->datasource);
		$this->getDb();
		printf("OK\n");

		$this->_last_key = 'TEST';
		$this->_last_record = $this->_sampleRecord();

		$quotes = $this->_last_record['cuotas'];
		printf("testing quote object decode:\n%s\ninto:\n",$quotes);
		foreach($this->decodeQuotes($quotes) as $quote=>$fields){
			list($contract, $val1, $val2, $date)=$fields;
			printf("%s = %s, %s, %s, %s\n",$quote,$contract, $val1, $val2, $date);
		}
		printf("\ndone\n");

		printf("test leerCliente()\n%s\nOK\n",json_encode($this->leerCliente('TEST')));	
		printf("test listarContratos()\n%s\nOK\n",json_encode($this->listarContratos('TEST')));	
		printf("test listarCuotas(test,021546)\n%s\nOK\n",json_encode($this->listarCuotas('TEST','021546')));	
		printf("test listarCuotas(test,021547)\n%s\nOK\n",json_encode($this->listarCuotas('TEST','021547')));	
		printf("test listarCuotas(test,021548)\n%s\nOK\n",json_encode($this->listarCuotas('TEST','021548')));	

	}
}
